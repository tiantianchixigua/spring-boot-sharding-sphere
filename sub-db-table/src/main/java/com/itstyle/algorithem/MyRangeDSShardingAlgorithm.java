package com.itstyle.algorithem;

import org.apache.shardingsphere.api.sharding.standard.RangeShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingValue;

import java.util.Arrays;
import java.util.Collection;

/**
 * 定义范围查询使用哪个库的算法,分片键范围查询会走这个方法
 * @author ken
 * @date 2023/10/20 15:30
 */
public class MyRangeDSShardingAlgorithm implements RangeShardingAlgorithm<Long> {

    @Override
    public Collection<String> doSharding(Collection<String> availableTargetNames, RangeShardingValue<Long> shardingValue) {
        Long upperVal = shardingValue.getValueRange().upperEndpoint();//100
        Long lowerVal = shardingValue.getValueRange().lowerEndpoint();//1
        String logicTableName = shardingValue.getLogicTableName();
        return Arrays.asList("ds0","ds1","ds2");
    }
}
