package com.itstyle.algorithem;

import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.math.BigInteger;
import java.util.Collection;

/**
 * 定义精确查询选择哪个库使用的算法
 * @author ken
 * @date 2023/10/20 15:25
 */
public class MyPreciseDSShardingAlgorithm implements PreciseShardingAlgorithm<Long> {

    /**
     * 对分片键取模
     * @param availableTargetNames
     * @param shardingValue
     * @return
     */
    @Override
    public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<Long> shardingValue) {
        //表名称
        String logicTableName = shardingValue.getLogicTableName();
        //表的分片键
        String columnName = shardingValue.getColumnName();
        //表的分片键对应的值
        Long cidValue = shardingValue.getValue();
        BigInteger shardingValueB = BigInteger.valueOf(cidValue);
        BigInteger resB = (shardingValueB.mod(new BigInteger("3")));
        String key = "ds"+resB;
        if(availableTargetNames.contains(key)){
            return key;
        }
        throw new UnsupportedOperationException("route "+ key +" is not supported ,please check your config");
    }
}
