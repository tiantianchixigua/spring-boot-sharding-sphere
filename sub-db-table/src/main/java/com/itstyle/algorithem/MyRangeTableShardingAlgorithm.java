package com.itstyle.algorithem;

import org.apache.shardingsphere.api.sharding.standard.RangeShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingValue;

import java.util.Arrays;
import java.util.Collection;

/**
 * 定义范围查询选择哪个表使用的算法，分片键范围查询会走这个方法
 *
 * @author ken
 * @date 2023/10/20 15:42
 */
public class MyRangeTableShardingAlgorithm implements RangeShardingAlgorithm<Long> {

    @Override
    public Collection<String> doSharding(Collection<String> availableTargetNames, RangeShardingValue<Long> shardingValue) {
        Long upperVal = shardingValue.getValueRange().upperEndpoint();//100
        Long lowerVal = shardingValue.getValueRange().lowerEndpoint();//1
        String logicTableName = shardingValue.getLogicTableName();
        //都查询
        return Arrays.asList(logicTableName + "_0", logicTableName + "_1", logicTableName + "_2");
    }
}
