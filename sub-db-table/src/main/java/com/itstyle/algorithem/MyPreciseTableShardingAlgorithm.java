package com.itstyle.algorithem;

import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;

/**
 * 定义表精确查询使用哪个表的算法
 *
 * @author ken
 * @date 2023/10/20 15:43
 */
public class MyPreciseTableShardingAlgorithm implements PreciseShardingAlgorithm<Long> {

    /**
     * @param availableTargetNames
     * @param shardingValue
     * @return
     */
    @Override
    public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<Long> shardingValue) {
        //表名
        String logicTableName = shardingValue.getLogicTableName();
        String columnName = shardingValue.getColumnName();
        //参数
        Long cidValue = shardingValue.getValue();
        //实现 userId / 库数量3  % 表数量3
        BigInteger shardingValueB = BigInteger.valueOf(cidValue);
        BigDecimal bigDecimal = new BigDecimal(shardingValueB.toString()).divideToIntegralValue(new BigDecimal("3"));
        BigInteger resB = BigInteger.valueOf(bigDecimal.longValue()).mod(new BigInteger("3"));
        String key = logicTableName + "_" + resB;
        if (availableTargetNames.contains(key)) {
            //返回这个参数走哪个表
            return key;
        }
        //couse_1, course_2
        throw new UnsupportedOperationException("route " + key + " is not supported ,please check your config");
    }
}
