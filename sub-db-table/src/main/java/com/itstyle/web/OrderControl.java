package com.itstyle.web;

import com.itstyle.common.id.SnowFlakeFactory;
import com.itstyle.entity.SeckillOrder;
import com.itstyle.mapper.SeckillGoodsOrderMapper;
import com.itstyle.mapper.SeckillUserOrderMapper;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Api(tags = "订单管理")
@RestController
@RequestMapping("/order")
public class OrderControl {


    @Resource
    private SeckillUserOrderMapper seckillUserOrderMapper;
    @Resource
    private SeckillGoodsOrderMapper seckillGoodsOrderMapper;

    /**
     * 插入
     */
    @PostMapping("/save")
    public void save() {
        Random random = new Random();
        for (Integer i = 0; i < 100; i++) {
            SeckillOrder seckillOrder = new SeckillOrder();
            seckillOrder.setId(SnowFlakeFactory.getSnowFlakeFromCache().nextId());
            seckillOrder.setUserId(i.longValue());
            seckillOrder.setGoodsId(i.longValue());
            seckillOrder.setGoodsName("秒杀商品");
            seckillOrder.setActivityPrice(new BigDecimal(random.nextInt(100)));
            seckillOrder.setQuantity(random.nextInt(10));
            seckillOrder.setOrderPrice(new BigDecimal(random.nextInt(100)));
            seckillOrder.setActivityId(1L);
            seckillOrder.setStatus(1);
            seckillOrder.setCreateTime(new Date());
            seckillUserOrderMapper.saveSeckillOrder(seckillOrder);
            seckillGoodsOrderMapper.saveSeckillOrder(seckillOrder);
        }
    }

    @PostMapping("/seckillUserOrderList")
    public List<SeckillOrder>  seckillUserOrderList(){
       return seckillUserOrderMapper.findList();
    }

    @GetMapping("/getSeckillOrderByUserId")
    public List<SeckillOrder>  getSeckillOrderByUserId(Long userId){
        return seckillUserOrderMapper.getSeckillOrderByUserId(userId);
    }

    /**
     * startTime不是分片键，不会走自定义的范围查询算法
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getSeckillOrderByCreateTime")
    public List<SeckillOrder>  getSeckillOrderByCreateTime(Long startTime,Long endTime){
        return seckillUserOrderMapper.getSeckillOrderByCreateTime(startTime,endTime);
    }

    /**
     * userId是分片键，会走自定义的范围查询算法
     * @param startUserId
     * @param endUserId
     * @return
     */
    @GetMapping("/getSeckillOrderByUserIdRange")
    public List<SeckillOrder>  getSeckillOrderByUserIdRange(Long startUserId,Long endUserId){
        return seckillUserOrderMapper.getSeckillOrderByUserIdRange(startUserId,endUserId);
    }



}
